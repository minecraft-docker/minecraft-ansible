CoreOS Ansible Role
===================

This role install pypy and all requirements to run Ansible on CoreOS.

Role Variables
--------------

```yaml
ansible_ssh_user: core
ansible_python_interpreter: "/opt/python/bin/python"
python_version: 3.5
pypy_version: 6.0.0
```

Example Playbook
----------------

The repository where pypy is hosted limits the number or parallel downloads. That's why a serial to 1 is set:
```yaml
- name: coreos-ansible
  hosts: coreos
  user: core
  become: yes
  gather_facts: False
  serial: 1
  roles:
    - coreos-ansible-bootstrap
```

On the hosts file, add vars:
```ini
[coreos]
coreos-01
coreos-02
coreos-03

[coreos:vars]
ansible_ssh_user: core
ansible_python_interpreter: "/opt/python/bin/python"
```

License
-------

GPLv3

Author Information
------------------

Pierre Mavro / deimosfr
Chris Weeks / chrisweeksnz
